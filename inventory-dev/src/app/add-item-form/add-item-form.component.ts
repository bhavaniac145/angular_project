import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormArray, FormGroup } from '@angular/forms';
import { InventoryService } from '../inventory.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Validators } from '@angular/forms';
@Component({
  selector: 'app-add-item-form',
  templateUrl: './add-item-form.component.html',
  styleUrls: ['./add-item-form.component.css']
})
export class AddItemFormComponent implements OnInit {
  public currentdate = new Date();
  myform = this.formBuilder.group({
    id: [''],
    productName: ['',[Validators.required,Validators.minLength(10)]],
    category: [''],
    availableUnits: ['',[Validators.required,Validators.minLength(10)]],
    unitPrice: [''],
    description: ['',[Validators.required,Validators.minLength(50)]],
    lastUpdateDate: [this.currentdate],
    location: this.formBuilder.group({
      city: ['',Validators.required],
      state: ['',Validators.required],
      country: ['',Validators.required],
    })
  });
  public items;
  public itemList = [];
  submit(attr) {
    this._itemService.addItems(attr)
      .subscribe(data => {
        console.log(data);
        this.router.navigate(['viewItems']);
      });
  }
  constructor(private formBuilder: FormBuilder, private _itemService: InventoryService,
              private router: Router, private location: Location) {
  }

  ngOnInit() {
  }

}
