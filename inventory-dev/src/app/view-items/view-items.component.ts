import { Component, OnInit, ViewChild } from '@angular/core';
import { InventoryService } from '../inventory.service';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSort } from '@angular/material';
@Component({
	selector: 'app-view-items',
	templateUrl: './view-items.component.html',
	styleUrls: ['./view-items.component.css']
})
export class ViewItemsComponent implements OnInit {

	public itemsList = [];
	userFilter: any = { city: '' };
	order = 'productName';
	public searchText = '';
	p = 1;
	selectedValue: string;
	updatePage(list) {
		const navigationExtras: NavigationExtras = {
			queryParams: {
				id: list.id,
				productName: list.productName,
				category: list.category,
				availableUnits: list.availableUnits,
				unitPrice: list.unitPrice,
				description: list.description,
				lastUpdateDate: list.lastUpdateDate,
				city: list.location.city,
				state: list.location.state,
				country: list.location.country

			}
		};
		this.router.navigate(['updateForm'], navigationExtras);
	}
	deletePage(attr) {
		// 	this.router.navigateByUrl("addItemForm", {skipLocationChange: true}).then(()=>{
		// this.router.navigate([decodeURI(this.location.path())]);
		// });
		this._inventoryService.deleteItem(attr)
			.subscribe(data => {
				console.log(data);
				this.router.navigate(['viewItems']);
			});
	}
	// @ViewChild(MatSort) sort: MatSort;

	constructor(private _inventoryService: InventoryService, private router: Router, private location: Location) {
	}

	ngOnInit() {
		// location.reload();
		this._inventoryService.getItems()
			.subscribe(data => this.itemsList = data);
		// this.itemsList.sort = this.sort;
	}

}
