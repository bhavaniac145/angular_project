import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemFormComponent } from './add-item-form/add-item-form.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ViewItemsComponent } from './view-items/view-items.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import { Location } from '@angular/common';

const routes: Routes = [
{path: 'navBar', component: NavBarComponent},
{path: 'searchBar', component: SearchBarComponent},
{path: 'addItemForm', component: AddItemFormComponent},
{path: 'viewItems', component: ViewItemsComponent},
{path: 'viewItems/:list', component: UpdateFormComponent},
{path: 'updateForm', component: UpdateFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
