import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Items } from './inventory';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  constructor(private http: HttpClient) { }
  private _url = 'http://localhost:5000/items-list';

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'my-auth-token'
    })
  };
  getItems(): Observable<Items[]> {
    return this.http.get<Items[]>(this._url);
  }
  addItems(items: Items) {
    return this.http.post<Items[]>(this._url, items);
  }

  updateItems(items: Items) {
    return this.http.put<Items[]>(`${'http://localhost:5000/items-list'}/${items.id}`, items, this.httpOptions);
  }

  deleteItem(id: number): Observable<{}> {
    const url1 = `${'http://localhost:5000/items-list'}/${id}`;
    return this.http.delete<Items[]>(url1, this.httpOptions);
  }
}
