import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormArray, FormGroup } from '@angular/forms';
import { InventoryService } from '../inventory.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
	selector: 'app-update-form',
	templateUrl: './update-form.component.html',
	styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
	public currentdate = new Date();
	public items;
	public itemList = [];
	public list_id;
	public list_productName;
	public list_category;
	public list_availableUnits;
	public list_unitPrice;
	public list_description;
	public list_lastUpdateDate = new Date();
	public list_city;
	public list_state;
	public list_country;
	myform = this.formBuilder.group({
		id: [''],
		productName: [''],
		category: [''],
		availableUnits: [''],
		unitPrice: [''],
		description: [''],
		lastUpdateDate: [''],
		location: this.formBuilder.group({
			city: [''],
			state: [''],
			country: [''],
		})
	});
	submit(attr) {
		this._itemService.updateItems(attr)
			.subscribe(data => {
				console.log(data);
				this.router.navigate(['viewItems']);
			});
	}

	constructor(private formBuilder: FormBuilder, private _itemService: InventoryService, private route: ActivatedRoute, private router: Router) {
	}

	ngOnInit() {
		this._itemService.getItems()
			.subscribe(data => this.itemList = data);
		(this.route.queryParams.subscribe(params => {
			this.list_id = params.id;
			this.list_productName = params.productName;
			this.list_category = params.category;
			this.list_availableUnits = params.availableUnits;
			this.list_unitPrice = params.unitPrice;
			this.list_lastUpdateDate = params.lastUpdateDate;
			this.list_description = params.description;
			this.list_city = params.city;
			this.list_state = params.state;
			this.list_country = params.country;

		}));
		this.myform.setValue({
			id: this.list_id,
			productName: this.list_productName,
			category: this.list_category,
			availableUnits: this.list_availableUnits,
			unitPrice: this.list_unitPrice,
			lastUpdateDate: this.list_lastUpdateDate,
			description: this.list_description,
			location: ({
				city: this.list_city,
				state: this.list_state,
				country: this.list_country,
			})
		});
	}

}
