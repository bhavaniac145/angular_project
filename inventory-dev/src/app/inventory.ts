export interface Items {
	id: number;
	productName: string;
	category: string;
	availableUnits: number;
	unitPrice: number;
	description: string;
	lastUpdateDate: string;
	city: string;
	state: string;
	country: string;
	location: object;
}
